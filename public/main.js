var frames = [
    {
        type: 'text',
        text: ["TAKE YOUR TIME TO THINK"],
        fadeIn: 0.5,
        duration: 3,
        fadeOut: 0.5,
        fontSize: 24,
        fontSizeEnd: 24,
        align: 'center',
        valign: 'bottom',
        textDelay: 0,
    },
    {
        type: 'img',
        link: "https://designshack.net/wp-content/uploads/placeholder-image.png",
        fadeIn: 0.5,
        duration: 3,
        fadeOut: 0.5,
        x: 350,
        y: 40,
        height: 'calc(100% - 80px)',
    },
    {
        type: 'img',
        link: "https://designshack.net/wp-content/uploads/placeholder-image.png",
        fadeIn: 0.5,
        duration: 3,
        fadeOut: 0.5,
        x: 0,
        y: 0,
        width: '100px',
    },
    {
        type: 'text',
        text: ["Imagine yourself.", "Ten years from now."],
        fadeIn: 0.5,
        duration: 5,
        fadeOut: 0.5,
        fontSize: 22,
        fontSizeEnd: 22,
        align: 'center',
        valign: 'middle',
        textDelay: 2,
    },
    {
        type: 'text',
        text: ["IN TEN YEARS"],
        fadeIn: 0.5,
        duration: 4,
        fadeOut: 0.5,
        fontSize: 36,
        fontSizeEnd: 72,
        align: 'center',
        valign: 'middle',
        textDelay: 0,
    },
    {
        type: 'text',
        text: ["Who will you be?"],
        fadeIn: 0.5,
        duration: 3,
        fadeOut: 0.5,
        fontSize: 22,
        fontSizeEnd: 22,
        align: 'center',
        valign: 'middle',
        textDelay: 0,
    },
    {
        type: 'text',
        text: ["Who will your friends be?"],
        fadeIn: 0.5,
        duration: 3,
        fadeOut: 0.5,
        fontSize: 22,
        fontSizeEnd: 22,
        align: 'center',
        valign: 'middle',
        textDelay: 0,
    },
    {
        type: 'text',
        text: ["Now think about your surroundings."],
        fadeIn: 0.5,
        duration: 3,
        fadeOut: 0.5,
        fontSize: 22,
        fontSizeEnd: 22,
        align: 'center',
        valign: 'middle',
        textDelay: 0,
    },
    {
        type: 'text',
        text: ["What will the world look like?"],
        fadeIn: 0.5,
        duration: 3,
        fadeOut: 0.5,
        fontSize: 22,
        fontSizeEnd: 22,
        align: 'center',
        valign: 'middle',
        textDelay: 0,
    },
    {
        type: 'text',
        text: ["End of experience."],
        fadeIn: 0.5,
        duration: 3,
        fadeOut: 0.5,
        fontSize: 22,
        fontSizeEnd: 22,
        align: 'center',
        valign: 'middle',
        textDelay: 0,
    }
]

var animation_speed = 500;
document.querySelector('#container tr').addEventListener('click', () => {
    hide_everything(show_text);
})

function playAudio() {
    var volume = 1;
    var muted = false;
    document.querySelector('#audio').innerHTML += `
    <img src="speaker-full.png" width="30px" height="30px" class="audioImage" id="audioImage"> 
    <input type="range" max="100" value="100" id="audioInput">`;

    window.updateSlider();
    document.querySelector('#audioImage').addEventListener('click', () => {
        if(muted) {
            $("audio").prop("volume", 0);
            $("#audioInput").val(volume*100).trigger("input");
            updateAudioImage(volume);
            muted = false;
        } else {
            $("audio").prop("volume", 0);
            $("#audioInput").val(0).trigger("input");
            document.querySelector('#audioImage').setAttribute('src', 'speaker-empty.png')
            muted = true;
        }
    });
    document.querySelector('#audioInput').addEventListener('input', (e) => {
        volume = e.target.value/100;
        $("audio").prop("volume", volume);
        updateAudioImage(volume);
    })

    $("audio").trigger('play');
    $('audio').animate({volume: 1}, 1000);
}

function stopAudio() {
    $('audio').animate({volume: 0}, 1000);
    setTimeout(() => {
        $("audio").trigger('pause');
        $('#audio').prop("currentTime",0);
        document.querySelector("#audioInput").outerHTML = '';
        document.querySelector("#audioImage").outerHTML = '';
    }, 1000);
}

function updateAudioImage(volume) {
    if (volume > 0.66) {
        document.querySelector('#audioImage').setAttribute('src', 'speaker-full.png')
    } else if (volume > 0.33) {
        document.querySelector('#audioImage').setAttribute('src', 'speaker-q3.png')
    } else if (volume > 0) {
        document.querySelector('#audioImage').setAttribute('src', 'speaker-q1.png')
    } else {
        document.querySelector('#audioImage').setAttribute('src', 'speaker-empty.png')
    }
}
function hide_everything(cb) {
    var content = d3.select("#index");
    content.select('#container tr').style('cursor', 'auto')
    content
        .transition()
        .duration(animation_speed)
        .ease(d3.easePoly)
        .style('opacity', 0)
        .on("end", ()=> {
            content.style('display', 'none');
            if (cb) cb();
        });
}

function show_everything(cb) {
    var content = d3.select("#index").style('display', 'block');
    content.select('#container tr').style('cursor', 'pointer');
    content
        .transition()
        .duration(animation_speed)
        .ease(d3.easePoly)
        .style('opacity', 1)
        .on("end", ()=> {
            if (cb) cb();
        });
}

function show_scrollable_content(cb) {
    document.getElementById('scrollable_content').style.display = "block";
    d3.select('#scrollable_content>.navbar').transition().duration(animation_speed).style('opacity',"1");
    window.currentSection = 1;
    show_scroll_section(undefined, window.currentSection);
    document.querySelector('body').addEventListener('wheel', scroll_handler)
    detectswipe('body',scroll_handler);

}


function scroll_handler(e) {
    console.log('eeee')
    console.log(e)
    if(!window.animation) return;
    let old = window.currentSection;
    if(e.deltaY > 0 && window.currentSection < document.querySelectorAll('#scrolls>div').length){
        window.currentSection = window.currentSection + 1
    } else if (e.deltaY < 0 && window.currentSection > 1) {
        window.currentSection = window.currentSection - 1
    }

    if(old != window.currentSection) {
        document.querySelector('body').removeEventListener('wheel', scroll_handler);
        show_scroll_section(old, window.currentSection);
        document.querySelector('body').addEventListener('wheel', scroll_handler)
        detectswipe('body',scroll_handler);
    }
}

function show_scroll_section(hide, show){
    window.animation = false;
    if(hide){
        d3.select(`#scrolls>div:nth-child(${hide})`)
        .style('opacity', 1)
        .style('margin-top', '0px')
        .transition()
        .duration(animation_speed)
        .ease(d3.easePoly)
        .style('opacity', 0)
        .style('margin-top', hide<show?'-30px':'30px')
        .on('end', function() {
            d3.select(this).style('display', 'none');
            d3.select(`#scrolls>div:nth-child(${show})`)
            .style('display', 'block')
        })
    } else {
        d3.select(`#scrolls>div:nth-child(${show})`)
        .style('display', 'block')
    }

    d3.select(`#scrolls>div:nth-child(${show})`)
    .style('opacity', 0)
    .style('margin-top', hide?hide<show?'30px':'-30px':'30px')
    .transition()
    .duration(animation_speed)
    .delay(animation_speed)
    .ease(d3.easePoly)
    .style('opacity', 1)
    .style('margin-top', '0px')
    .on('end', function() {
        window.animation = true;
    })
}

function show_text() {
    playAudio();
    display_frame(0, frames.length, () => {show_scrollable_content(show_everything); stopAudio();});
}

function display_frame(i, maxIter, cb){
    if(i >= maxIter) {
        cb();
        return;
    }

    let clicked = false;
    var experience = d3.select("#experience");
    
           
    if(frames[i].type == 'text'){
        let td = experience
        .append("table")
        .attr("class", "fullscreen")
        .attr("id", "currentTable")
        .append('tr')
            .append('td')
                .attr('align', frames[i].align)
                .attr('valign', frames[i].valign)
        let texts = [];
        for (let text of frames[i].text){
            texts.push(td
                .append("p")
                    .text(text)
                    .style("opacity", 0)
                    .style("font-size", frames[i].fontSize));
            td.append('br')
        }
                
    
        document.getElementById('currentTable').addEventListener('click', () => {
            clicked = true;
            for (let z = 1; z < texts.length; z++) {
                texts[z]
                    .transition()
                    .duration(frames[i].fadeOut*1000)
                    .ease(d3.easePoly)
                    .style("opacity", 0)
            }
            texts[0]
                .transition()
                .duration(frames[i].fadeOut*1000)
                .ease(d3.easePoly)
                .style("opacity", 0)
                .on('end', () => {
                    experience.html('');
                    display_frame(i+1, maxIter, cb);
                })
        });
        
    
        for (let tc = 1; tc < texts.length; tc++) {
            texts[tc]
                .transition()
                .delay(frames[i].textDelay * tc * 1000)
                .duration(frames[i].fadeIn*1000)
                .ease(d3.easePoly)
                .style("opacity", 1)
                .style('transform', `scale(${frames[i].fontSize/frames[i].fontSizeEnd})`)
                .style('font-size', frames[i].fontSizeEnd)
                .on('end', () => {
                    texts[tc]
                        .transition()
                        .duration(frames[i].duration*1000)
                        // .style('font-size', frames[i].fontSizeEnd)
                        .style('transform', `scale(1)`)
                })
        }
    
        console.log(frames[i].fontSizeEnd/frames[i].fontSize)
        texts[0]
            .style('text-align', 'center')
            .style('transform', `scale(${frames[i].fontSize/frames[i].fontSizeEnd})`)
            .style('-webkit-transition', `transform ${frames[i].duration}s ease`)
            .style('-moz-transition', `transform ${frames[i].duration}s ease`)
            .style('transition', `transform ${frames[i].duration}s ease`)
            .style('font-size', frames[i].fontSizeEnd)
            .transition()
            .duration(frames[i].fadeIn*1000)
            .ease(d3.easePoly)
            .style("opacity", 1)
            .on('end', () => {
                texts[0]
                    // .transition()
                    // .duration(frames[i].duration*1000)
                .style('transform', `scale(1)`)
                setTimeout(() => {
                    if(clicked) return;
                    for (let z = 1; z < texts.length; z++) {
                        texts[z]
                        .transition()
                        .duration(frames[i].fadeOut*1000)
                        .ease(d3.easePoly)
                        .style("opacity", 0)
                        
                    }
                    texts[0]
                    .transition()
                    .duration(frames[i].fadeOut*1000)
                    .ease(d3.easePoly)
                    .style("opacity", 0)
                    .on('end', () => {
                        experience.html('');
                        display_frame(i+1, maxIter, cb);
                    })
                }, frames[i].duration*1000);
            });
    } else {
        imgelem = experience
                .append("img")
                    .attr("id", "currentTable")
                    .attr('src', frames[i].link)
                    .style("opacity", 0)
                    .style("position", 'absolute')
                    .style("top", frames[i].y)
                    .style("left", frames[i].x)
                    .style(frames[i].width?"width":"height", frames[i].width?frames[i].width:frames[i].height)
             
        document.getElementById('currentTable').addEventListener('click', () => {
            clicked = true;
            imgelem
                .transition()
                .duration(frames[i].fadeOut*1000)
                .ease(d3.easePoly)
                .style("opacity", 0)
                .on('end', () => {
                    experience.html('');
                    display_frame(i+1, maxIter, cb);
                })
        });
        
    
        
        imgelem
            .transition()
            .duration(frames[i].fadeIn*1000)
            .ease(d3.easePoly)
            .style("opacity", 1)
            .on('end', () => {
                setTimeout(() => {
                    if(clicked) return;
                    
                    imgelem
                    .transition()
                    .duration(frames[i].fadeOut*1000)
                    .ease(d3.easePoly)
                    .style("opacity", 0)
                    .on('end', () => {
                        experience.html('');
                        display_frame(i+1, maxIter, cb);
                    })
                }, frames[i].duration*1000);
            });
    }
    
}

function detectswipe(el,func) {
    swipe_det = new Object();
    swipe_det.sX = 0;
    swipe_det.sY = 0;
    swipe_det.eX = 0;
    swipe_det.eY = 0;
    var min_x = 20;  //min x swipe for horizontal swipe
    var max_x = 40;  //max x difference for vertical swipe
    var min_y = 40;  //min y swipe for vertical swipe
    var max_y = 50;  //max y difference for horizontal swipe
    var direc = "";
    ele = document.querySelector(el);
    ele.addEventListener('touchstart',function(e){
      var t = e.touches[0];
      swipe_det.sX = t.screenX; 
      swipe_det.sY = t.screenY;
    },false);
    ele.addEventListener('touchmove',function(e){
    //   e.preventDefault();
      var t = e.touches[0];
      swipe_det.eX = t.screenX; 
      swipe_det.eY = t.screenY;    
    },false);
    ele.addEventListener('touchend',function(e){
      //horizontal detection
      if ((((swipe_det.eX - min_x > swipe_det.sX) || (swipe_det.eX + min_x < swipe_det.sX)) && ((swipe_det.eY < swipe_det.sY + max_y) && (swipe_det.sY > swipe_det.eY - max_y)))) {
        if(swipe_det.eX > swipe_det.sX) direc = "r";
        else direc = "l";
      }
      //vertical detection
      if ((((swipe_det.eY - min_y > swipe_det.sY) || (swipe_det.eY + min_y < swipe_det.sY)) && ((swipe_det.eX < swipe_det.sX + max_x) && (swipe_det.sX > swipe_det.eX - max_x)))) {
        if(swipe_det.eY > swipe_det.sY) direc = "d";
        else direc = "u"
      }
  
      if (direc != "") {
        if(typeof func == 'function') func({deltaY: direc=="u"?1:direc=="d"?-1:0});
      }
      direc = "";
    },false);  
  }
